# Codebase release 2.0 for ALF (Algorithms for Lattice Fermions)

by F. F. Assaad, M. Bercx, F. Goth, A. Götz, J. S. Hofmann, E. Huffman, Z. Liu, F. Parisen Toldin, J. S. E. Portela, J. Schwab

SciPost Phys. Codebases 1-r2.0 (2022) - published 2022-08-22

[DOI:10.21468/SciPostPhysCodeb.1-r2.0](https://doi.org/10.21468/SciPostPhysCodeb.1-r2.0)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.1-r2.0) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright F. F. Assaad, M. Bercx, F. Goth, A. Götz, J. S. Hofmann, E. Huffman, Z. Liu, F. Parisen Toldin, J. S. E. Portela, J. Schwab

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.1-r2.0](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.1-r2.0)
* Live (external) repository at [https://git.physik.uni-wuerzburg.de/ALF/ALF/-/tree/ALF-2.0](https://git.physik.uni-wuerzburg.de/ALF/ALF/-/tree/ALF-2.0)

