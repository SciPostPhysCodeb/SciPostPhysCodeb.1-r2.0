% Copyright (c) 2016 The ALF project.
% This is a part of the ALF project documentation.
% The ALF project documentation by the ALF contributors is licensed
% under a Creative Commons Attribution-ShareAlike 4.0 International License.
% For the licensing details of the documentation see license.CCBYSA

% !TEX root = doc.tex
%------------------------------------------------------------
\subsection{File structure}\label{sec:files}
%------------------------------------------------------------
%
\begin{table}[h]
	\begin{center}
	\begin{tabular}{@{} p{0.4\columnwidth} p{0.57\columnwidth} @{}}\toprule
   	Directory                             & Description \\\midrule
   	\path{Prog/}                          & Main program and subroutines  \\
   	\path{Libraries/}                     & Collection of mathematical routines \\  
  	\path{Analysis/}                      & Routines for error analysis \\
  	\path{Scripts_and_Parameters_files/}  & Helper scripts and the \path{Start/} directory, which contains the files required to start a run \\
  	\path{Documentation/}                 & This documentation\\
	\path{Mathematica/}                   & Mathematica notebooks to  evaluate higher order correlation functions with Wicks theorem \\
  	\path{testsuite/}                     & An automatic test suite for various parts of the code\\ \bottomrule
  	%\hline
	\end{tabular}
   	\caption{Overview of the directories included in the ALF package.\label{table:files}}
   \end{center}
\end{table}
%

The code package, summarized in Table~\ref{table:files}, consists of the program directories \path{Prog/}, \path{Libraries/}, \path{Analysis/}, and the directory \path{Scripts_and_Parameters_files/}, which contains supporting scripts and, in its subdirectory \path{Start}, the input files necessary for a run, described in the Sec.~\ref{sec:input}  as well as \path{Mathematica/}   that contains 
Mathematica notebooks to  evaluate higher order correlation functions with Wicks theorem  as  described in Appendix  \ref{Wick_appendix}. \\
The routines available in the directory \path{Analysis/} are described in Sec.~\ref{sec:analysis}, and the testsuite in Sec.~\ref{sec:compilation}. 

Below we describe the structure of ALF's input and output files. Notice that the input/output files for the Analysis routines are described in Sec.~\ref{sec:analysis}.

%------------------------------------------------------------
\subsubsection{Input files}\label{sec:input}
%------------------------------------------------------------
%

The package's two input files are described in Table~\ref{table:input}.
%
\begin{table}[h]
	\begin{center}
		\begin{tabular}{@{} l l @{}}\toprule
			File & Description \\\midrule
			\path{parameters} &  Sets the parameters for: lattice, model, QMC process, and error analysis\\
			\path{seeds} & List of integer numbers to initialize the random number generator and \\
			& to start a simulation from scratch
			\\\bottomrule
		\end{tabular}
		\caption{Overview of the input files required for a simulation, which can be found in the subdirectory \texttt{Scripts\_and\_Parameters\_files/Start/}. \label{table:input}}
	\end{center}
\end{table}
%
The parameter file \path{Start/parameters} has the following form --
using as an example the Hubbard model on a square lattice (see Sec.~\ref{sec:hubbard} for the general SU(N) Hubbard and Sec.~\ref{sec:vanilla} for a detailed walk-through on its plain vanilla version):
%
\begin{lstlisting}[style=fortran,escapechar=\#,breaklines=true]
!=======================================================================================
!  Input variables for a general ALF run
!---------------------------------------------------------------------------------------

&VAR_lattice               !! Parameters defining the specific lattice and base model
L1           = 6            ! Length in direction a_1
L2           = 6            ! Length in direction a_2
Lattice_type = "Square"     ! Sets a_1 = (1,0), a_2=(0,1), Norb=1, N_coord=2
Model        = "Hubbard"    ! Sets the Hubbard model, to be specified in &VAR_Hubbard
/

&VAR_Model_Generic         !! Common model parameters
Checkerboard = .T.          ! Whether checkerboard decomposition is used
Symm         = .T.          ! Whether symmetrization takes place
N_SUN        = 2            ! Number of colors
N_FL         = 1            ! Number of flavors
Phi_X        = 0.d0         ! Twist along the L_1 direction, in units of the flux quanta
Phi_Y        = 0.d0         ! Twist along the L_2 direction, in units of the flux quanta
Bulk         = .T.          ! Twist as a vector potential (.T.); at the boundary (.F.)
N_Phi        = 0            ! Total number of flux quanta traversing the lattice
Dtau         = 0.1d0        ! Thereby Ltrot=Beta/dtau
Beta         = 5.d0         ! Inverse temperature
Projector    = .F.          ! Whether the projective algorithm is used
Theta        = 10.d0        ! Projection parameter
/

&VAR_QMC                   !! Variables for the QMC run
Nwrap                = 10   ! Stabilization. Green functions will be computed from 
                            ! scratch after each time interval Nwrap*Dtau
NSweep               = 20   ! Number of sweeps
NBin                 = 5    ! Number of bins
Ltau                 = 1    ! 1 to calculate time-displaced Green functions; 0 otherwise
LOBS_ST              = 0    ! Start measurements at time slice LOBS_ST
LOBS_EN              = 0    ! End measurements at time slice LOBS_EN
CPU_MAX              = 0.0  ! Code stops after CPU_MAX hours, if 0 or not
                            ! specified, the code stops after Nbin bins
Propose_S0           = .F.  ! Proposes single spin flip moves with probability exp(-S0) 
Global_moves         = .F.  ! Allows for global moves in space and time 
N_Global             = 1    ! Number of global moves per sweep 
Global_tau_moves     = .F.  ! Allows for global moves on a single time slice.  
N_Global_tau         = 1    ! Number of global moves that will be carried out on a 
                            ! single time slice
Nt_sequential_start  = 0    ! One can combine sequential & global moves on a time slice
Nt_sequential_end    = -1   ! The program then carries out sequential local moves in the
                            ! range [Nt_sequential_start, Nt_sequential_end] followed by
                            ! N_Global_tau global moves
Langevin             = .F.  ! Langevin update
Delta_t_Langevin_HMC = 0.01 ! Default time step for Langevin and HMC updates
Max_Force            = 1.5  ! Max Force for  Langevin
/

&VAR_errors                !! Variables for analysis programs
n_skip  = 1                 ! Number of bins that to be skipped
N_rebin = 1                 ! Rebinning  
N_Cov   = 0                 ! If set to 1 covariance computed for non-equal-time
                            ! correlation functions
N_auto   = 0                ! If > 0  triggers  calculation of autocorrelation 
N_Back   = 1                ! If set to 1, substract background in correlation functions
/  

&VAR_TEMP                  !! Variables for parallel tempering
N_exchange_steps      = 6   ! Number of exchange moves #[see Eq.~\eqref{eq:exchangestep}]#
N_Tempering_frequency = 10  ! The frequency in units of sweeps at which the
                            ! exchange moves are carried out
mpi_per_parameter_set = 2   ! Number of mpi-processes per parameter set
Tempering_calc_det    = .T. ! Specifies whether the fermion weight has to be taken
                            ! into account while tempering. The default is .true.,
                            ! and it can be set to .F. if the parameters that
                            ! get varied only enter the free bosonic action S_0
/

&VAR_Max_Stoch             !! Variables for Stochastic Maximum entropy
Ngamma     = 400            ! Number of Dirac delta-functions for parametrization
Om_st      = -10.d0         ! Frequency range lower bound
Om_en      = 10.d0          ! Frequency range upper bound
NDis       = 2000           ! Number of boxes for histogram
Nbins      = 250            ! Number of bins for Monte Carlo
Nsweeps    = 70             ! Number of sweeps per bin
NWarm      = 20             ! The Nwarm first bins will be ommitted
N_alpha    = 14             ! Number of temperatures
alpha_st   = 1.d0           ! Smallest inverse temperature increment for inverse
R          = 1.2d0          ! temperature (see above) 
Checkpoint = .F.            ! Whether to produce dump files, allowing the simulation
                            ! to be resumed later on
Tolerance  = 0.1d0          ! Data points for which the relative error exceeds the
                            ! tolerance threshold will be omitted.
/

&VAR_Hubbard               !! Variables for the specific model
Mz         = .T.            ! When true, sets the M_z-Hubbard model: Nf=2, demands that
                            ! N_sun is even, HS field couples to the z-component of
                            ! magnetization; otherwise, HS field couples to the density
Continuous = .F.            ! Uses (T: continuous; F: discrete) HS transformation
ham_T      = 1.d0           ! Hopping parameter
ham_chem   = 0.d0           ! Chemical potential
ham_U      = 4.d0           ! Hubbard interaction
ham_T2     = 1.d0           ! For bilayer systems
ham_U2     = 4.d0           ! For bilayer systems
ham_Tperp  = 1.d0           ! For bilayer systems
/
               
\end{lstlisting}
%
\FloatBarrier

The program allows for a number of different  updating schemes.  If no other variables are specified in the \texttt{VAR\_QMC} name space, then the program will run in its default mode, namely the sequential single spin-flip mode.
In particular, note that if \texttt{Nt\_sequential\_start}  and \texttt{Nt\_sequential\_end}  are not specified and that the variable \texttt{Global\_tau\_moves}  is set to true, then  the program will  carry out only global moves, by setting \texttt{Nt\_sequential\_start=1}  and \texttt{Nt\_sequential\_end=0}. 

%If the program is not compiled with the parallel tempering flag, then the \texttt{VAR\_TEMP} name space can be omitted from the parameter file.


%------------------------------------------------------------
\subsubsection{Output files -- observables} \label{sec:output_obs}
%------------------------------------------------------------
%
The standard output files are listed in Table~\ref{table:output}. Notice that, besides these files, which contain direct QMC outputs, ALF can also produce a number of analysis output files, discussed in Sec.~\ref{sec:analysis}.

The output of the measured data is organized in bins. One bin corresponds to the arithmetic average over a fixed number of individual measurements which depends on the chosen measurement interval \path{[LOBS_ST,LOBS_EN]} on the imaginary-time axis and on the number \path{NSweep} of Monte Carlo sweeps. If the user runs an MPI parallelized version of the code, the average also extends over the number of MPI threads.
%
\begin{table}[h]
	\begin{center}
   \begin{tabular}{@{} p{0.3\columnwidth}p{0.67\columnwidth} @{}}\toprule
   File               & Description \\\midrule
   \path{info}        & After completion of the simulation, this file documents the parameters of the model, as well as the QMC run and simulation metrics (precision, acceptance rate, wallclock time)\\
   \path{X_scal}      & Results of equal-time measurements of scalar observables \\
   & The placeholder \path{X} stands for the observables \path{Kin}, \path{Pot}, \path{Part}, and \path{Ener} \\
   \path{X_scal_info} & Contains info on how to analyze the observable and optionally a description.\\
   \path{Y_eq, Y_tau} & Results of equal-time and time-displaced measurements of correlation functions. The placeholder \path{Y} stands for \path{Green}, \path{SpinZ}, \path{SpinXY}, \path{Den}, etc. \\   
   \path{Y_eq_info, Y_tau_info} & Additional info, like Bravais lattice and unit cell, for equal-time and time-displaced observables \\
   \path{confout_<thread number>} & Output files (one per MPI instance) for the HS and bosonic configuration \\\bottomrule
   \end{tabular}
   \caption{Overview of the standard output files. See Sec.~\ref{sec:obs} for the definitions of observables and correlation functions. \label{table:output}}
\end{center}
\end{table}


The formatting of a single bin's output depends on the observable type, \path{Obs_vec} or \path{Obs_Latt}:
\begin{itemize}
\item Observables of type \path{Obs_vec}:
For each additional bin, a single new line is added to the output file.
In case of an observable with \path{N_size} components, the formatting is 
{\small
\begin{verbatim}
N_size+1  <measured value, 1> ... <measured value, N_size>  <measured sign>
\end{verbatim}
}
The counter variable \path{N_size+1} refers to the number of measurements per line, including the phase measurement. 
This format is required by the error analysis routine (see Sec.~\ref{sec:analysis}). 
Scalar observables like kinetic energy, potential energy, total energy and particle number are treated as a vector 
of size \path{N_size=1}.

\item Observables of type \path{Obs_Latt}:
For each additional bin, a new data block is added to the output file. 
The block consists of the expectation values [Eq.~(\ref{eqn:o})] contributing to the background part [Eq.~(\ref{eqn:s_back})] of the correlation function,
and the correlated part [Eq.~(\ref{eqn:s_corr})] of the correlation function.
For imaginary-time displaced correlation functions, the formatting of the block is given by:
{\small
\begin{alltt}
<measured sign> <N_orbital> <N_unit_cell> <N_time_slices> <dtau> <Channel>
do alpha = 1, N_orbital
   \(\langle\hat{O}\sb{\alpha}\rangle \)
enddo
do i = 1, N_unit_cell
   <reciprocal lattice vector k(i)>
   do tau = 1, N_time_slices
      do alpha = 1, N_orbital
         do beta = 1, N_orbital
            \(\langle{S}\sb{\alpha,\beta}\sp{(\mathrm{corr})}(k(i),\tau)\rangle\)
         enddo
      enddo
   enddo
enddo
\end{alltt}
}
The same block structure is used for equal-time correlation functions, except for the entries  \path{<N_time_slices>}, \path{<dtau>} and \path{<Channel>}, which are then omitted.
Using this structure for the bins as input, the full correlation function $S_{\alpha,\beta}(\vec{k},\tau)$ [Eq.~(\ref{eqn:s})] is then calculated by calling the error analysis routine (see Sec.~\ref{sec:analysis}).
\end{itemize}


%------------------------------------------------------------
%\subsection{Scripts}\label{sec:scripts}
%------------------------------------------------------------
%

%\begin{table}[h]
%   \begin{tabular}{@{} l l l @{}}\toprule
%   Script & Description & Section\\\midrule
%   \path{Start/analysis.sh} & Starts the error analysis. & \ref{sec:analysis}\\
%   \path{Start/out_to_in.sh} & Copies outputted field configurations into input files for further runs. & \ref{sec:running} \\\bottomrule
%   \end{tabular}
%   \caption{Overview of the bash script files and the corresponding reference sections. 
%      \label{table:scripts}}
%\end{table}
%
