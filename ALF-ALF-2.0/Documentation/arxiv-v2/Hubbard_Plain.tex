% Copyright (c) 2016-2019 The ALF project.
% This is a part of the ALF project documentation.
% The ALF project documentation by the ALF contributors is licensed
% under a Creative Commons Attribution-ShareAlike 4.0 International License.
% For the licensing details of the documentation see license.CCBYSA.

% !TEX root = doc.tex

All the data structures necessary to implement a given model have been introduced in the previous sections. Here we show how to implement the Hubbard model  by specifying the lattice, the hopping, the interaction, the trial wave function  (if  required), and the observables.  Consider  the  \textit{plain vanilla}  Hubbard model  written as: 
\begin{equation}
\label{eqn_hubbard_Mz}
\mathcal{H}=
- t 
\sum\limits_{\langle \ve{i}, \ve{j} \rangle,  \sigma={\uparrow,\downarrow}} 
  \left(  \hat{c}^{\dagger}_{\ve{i}, \sigma} \hat{c}^{\phantom\dagger}_{\ve{j},\sigma}  + \hc \right) 
- \frac{U}{2}\sum\limits_{\ve{i}}\left[
\hat{c}^{\dagger}_{\ve{i}, \uparrow} \hat{c}^{\phantom\dagger}_{\ve{i}, \uparrow}  -   \hat{c}^{\dagger}_{\ve{i}, \downarrow} \hat{c}^{\phantom\dagger}_{\ve{i}, \downarrow}  \right]^{2}   
-  \mu \sum_{\ve{i},\sigma } \hat{c}^{\dagger}_{\ve{i}, \sigma}  \hat{c}^{\phantom\dagger}_{\ve{i},\sigma}. 
\end{equation} 
Here $ \langle \ve{i}, \ve{j} \rangle $    denotes nearest neighbors. 
We can make contact with the general form of the Hamiltonian  [see Eq.~\eqref{eqn:general_ham}] by setting: 
$N_{\mathrm{fl}} = 2$, $N_{\mathrm{col}} \equiv \texttt{N\_SUN}     =1 $, 
 $M_T    =    1$, 
 \begin{equation}
  T^{(ks)}_{x y}   = 
  \left\{ 
 \begin{array}{ll}
       -t         & \text{if } x,y \text{ are nearest neighbors} \\
       -\mu    & \text{if } x = y \\
       0         &  \text{otherwise},
 \end{array}
  \right.
 \end{equation}
 $M_V   =  N_{\text{unit-cell}} $,  $U_{k}       =   \frac{U}{2}$, 
 $V_{x y}^{(k, s=1)} =  \delta_{x,y} \delta_{x,k}  $,  $V_{x y}^{(k, s=2)} =  - \delta_{x,y} \delta_{x,k}  $,  $\alpha_{ks}   = 0  $ and $M_I       = 0 $.   
The coupling of the HS fields to the $z$-component of the magnetization breaks the SU(2) spin symmetry. Nevertheless, the $z$-component of the spin remains a good quantum number such that the imaginary-time propagator -- for a given HS field -- is block  diagonal in this quantum number. This corresponds to the flavor index running from $1$ to $2$,  labeling spin up and spin down degrees of freedom. We note that  in this formulation the  hopping matrix can be flavor dependent such that a Zeeman  magnetic field can be introduced.  If the chemical potential is set to zero, this will not generate a negative sign problem \cite{Wu04,Milat04,Bercx09}.    
The code that we describe below  can be found in the module \path{Prog/Hamiltonians/Hamiltonian_plain_vanilla_hubbard_mod.F90}. This file may be a good starting point for implementing a new model Hamiltonian. 

%------------------------------------------------------------
\subsection{Setting the Hamiltonian:  \texttt{Ham\_set} }
%------------------------------------------------------------

The main program will call the subroutine \texttt{Ham\_set} in the module \path{Hamiltonian_plain_vanilla_hubbard_mod.F90}.
The latter  subroutine  defines the  public variables
\begin{lstlisting}[style=fortran]
Type(Operator),     dimension(:,:), allocatable :: Op_V  ! Interaction
Type(Operator),     dimension(:,:), allocatable :: Op_T  ! Hopping
Type(WaveFunction), dimension(:),   allocatable :: WF_L  ! Left trial wave function
Type(WaveFunction), dimension(:),   allocatable :: WF_R  ! Right trial wave function
Type(Fields)        :: nsigma                            ! Fields
Integer             :: Ndim                              ! Number of sites
Integer             :: N_FL                              ! number of flavors
Integer             :: N_SUN	                         ! Number of colors 
Integer             :: Ltrot                             ! Total number of trotter silces
Integer             :: Thtrot                            ! Number of trotter slices 
                                                         ! reserved for projection
Logical             :: Projector                         ! Projector code
Integer             :: Group_Comm                        ! Group communicator for MPI
Logical             :: Symm                              ! Symmetric trotter 
\end{lstlisting}
which specify the model.  The  routine \texttt{Ham\_set}  will first  read the parameter file \texttt{parameters} (see Sec.~\ref{sec:input}); then set the lattice: \texttt{Call Ham\_latt};  set the hopping: \texttt{Call Ham\_hop};  
 set the interaction: \texttt{call Ham\_V}; and if required, set the trial wave function: \texttt{call Ham\_trial}.

%------------------------------------------------------------
\subsection{The lattice: \texttt{Ham\_latt}} \label{U_PV_Ham_latt}
%------------------------------------------------------------

The routine, which sets the square lattice, reads:
\begin{lstlisting}[style=fortran]
a1_p(1) = 1.0  ; a1_p(2) = 0.d0
a2_p(1) = 0.0  ; a2_p(2) = 1.d0
L1_p    = dble(L1)*a1_p
L2_p    = dble(L2)*a2_p
Call Make_Lattice(L1_p, L2_p, a1_p, a2_p, Latt)
Latt_unit%Norb = 1
Latt_unit%N_coord = 2
allocate(Latt_unit%Orb_pos_p(Latt_unit%Norb,2))
Latt_unit%Orb_pos_p(1, :) = [0.d0, 0.d0]
Ndim = Latt%N*Latt_unit\%Norb

\end{lstlisting}
In its last line, the routine sets the total number of single particle states per flavor and color:
\texttt{Ndim = Latt\%N*Latt\_unit\%Norb}.

%------------------------------------------------------------
\subsection{The hopping: \texttt{Ham\_hop}} \label{U_PV_Ham_hop}
%------------------------------------------------------------

The hopping matrix is implemented as follows. 
We allocate an array of dimension $1\times 1$ of type operator  called \texttt{Op\_T} and set the  dimension for the hopping  matrix to $N=N_{\mathrm{dim}}$. The operator allocation and initialization is performed by the subroutine \texttt{Op\_make}: 
\begin{lstlisting}[style=fortran]
call Op_make(Op_T(1,1),Ndim);  call Op_make(Op_T(1,2),Ndim) 
\end{lstlisting}
Since the hopping  does not  break down into small blocks, we have ${\bm P}=\mathds{1}$   and  
\begin{lstlisting}[style=fortran]
Do nf = 1, N_FL
  Do i = 1,Latt%N
     Op_T(1,nf)%P(i) = i
  Enddo
Enddo
\end{lstlisting}
We set the hopping matrix  with 
\begin{lstlisting}[style=fortran]
Do nf = 1, N_FL
   Do I = 1, Latt%N
      Ix = Latt%nnlist(I,1,0)
      Iy = Latt%nnlist(I,0,1)
      Op_T(1,nf)%O(I,  Ix) = cmplx(-Ham_T,    0.d0, kind(0.D0))
      Op_T(1,nf)%O(Ix, I ) = cmplx(-Ham_T,    0.d0, kind(0.D0))
      Op_T(1,nf)%O(I,  Iy) = cmplx(-Ham_T,    0.d0, kind(0.D0))
      Op_T(1,nf)%O(Iy, I ) = cmplx(-Ham_T,    0.d0, kind(0.D0))
      Op_T(1,nf)%O(I,  I ) = cmplx(-Ham_chem, 0.d0, kind(0.D0))
   Enddo
   Op_T(1,nf)%g     = -Dtau
   Op_T(1,nf)%alpha = cmplx(0.d0,0.d0, kind(0.D0))
   Call Op_set(Op_T(1,nf))
Enddo
\end{lstlisting}
Here, the integer function \texttt{Latt\%nnlist(I,n,m)} is defined in the lattice module and returns the index of the lattice site $ \vec{I} +  n \vec{a}_1 +  m \vec{a}_2$.
Note that periodic boundary conditions are 
already taken into account.  The hopping parameter \texttt{Ham\_T}, as well as the chemical potential \texttt{Ham\_chem} are read from the parameter file.  
To completely define the hopping  we further set: \texttt{Op\_T(1,nf)\%g = -Dtau }, \texttt{Op\_T(1,nf)\%alpha = cmplx(0.d0,0.d0, kind(0.D0))} and call the routine  \texttt{Op\_set(Op\_T(1,nf))}  so as to generate  the unitary transformation and eigenvalues as specified in Table \ref{table:operator}.  Recall that for the hopping, the variable  \texttt{Op\_set(Op\_T(1,nf))\%type}  takes its default value of 0.  
Finally, note that, although a checkerboard decomposition is not used here, it can be implemented by considering a larger number of sparse hopping matrices.  


%------------------------------------------------------------
\subsection{The interaction: \texttt{Ham\_V}}\label{U_PV_Ham_V} 
%------------------------------------------------------------
To implement  the interaction, we allocate an array of \texttt{Operator} type. The array is called  \texttt{Op\_V} and has dimensions $N_{\mathrm{dim}}\times N_{\mathrm{fl}}=N_{\mathrm{dim}} \times 2$. 
We set the dimension for the interaction term to  $N=1$, and  allocate and initialize this array of type  \texttt{Operator} by repeatedly calling the subroutine \texttt{Op\_make}: 

\begin{lstlisting}[style=fortran]
Allocate(Op_V(Ndim,N_FL))
do nf = 1,N_FL
   do i  = 1, Ndim
      Call Op_make(Op_V(i,nf), 1)
   enddo
enddo
Do nf = 1,N_FL
   X = 1.d0
   if (nf == 2)  X = -1.d0
   Do i = 1,Ndim
      nc = nc + 1
      Op_V(i,nf)%P(1)   = I
      Op_V(i,nf)%O(1,1) = cmplx(1.d0, 0.d0, kind(0.D0))
      Op_V(i,nf)%g      = X*SQRT(CMPLX(DTAU*ham_U/2.d0, 0.D0, kind(0.D0))) 
      Op_V(i,nf)%alpha  = cmplx(0.d0, 0.d0, kind(0.D0))
      Op_V(i,nf)%type   = 2
      Call Op_set( Op_V(i,nf) )
   Enddo
Enddo
\end{lstlisting}
The code above makes it explicit that there is a sign difference between the coupling of the HS field in the two flavor sectors. 

%------------------------------------------------------------
\subsection{The trial wave function: \texttt{Ham\_Trial}} \label{U_PV_Ham_Trial}
%------------------------------------------------------------
\label{Sec:Plain_vanilla_trial}
As  argued in Sec.~\ref{sec:trial_wave_function}, it is useful to generate the trial wave function from a non-interacting trial Hamiltonian.   Here we will  use the same left and right  flavor-independent trial wave functions that correspond to the ground state of: 
\begin{equation}
   \hat{H}_T    = - t \sum_{\ve{i}} \left[  \left( 1 + (-1)^{i_x + i_y}  \delta \right)  \hat{c}^{\dagger}_{\ve{i}}   \hat{c}^{\phantom\dagger}_{\ve{i} +\ve{a}_x}  +  
   							\left(1 - \delta \right)  \hat{c}^{\dagger}_{\ve{i}}   \hat{c}^{\phantom\dagger}_{\ve{i} +\ve{a}_y}    + \hc  \right]   \equiv   \sum_{\ve{i},\ve{j}}  \hat{c}^{\dagger}_{\ve{i}}   h_{\ve{i},\ve{j}}  \hat{c}^{\phantom\dagger}_{\ve{i}}.
\end{equation}
For the half-filled case, the  dimerization $\delta  = 0^{+} $  opens up a gap at  half-filling,   thus generating the desired  non-degenerate  trial wave function  that has the same symmetries (particle-hole  for instance) as  the   trial  Hamiltonian.

Diagonalization  of  $ h_{\ve{i},\ve{j}}$,      $U^{\dagger} h  U  = \mathrm{Diag} \left(   \epsilon_1, \cdots, \epsilon_{N_{\mathrm{dim}}} \right) $     with  $\epsilon_i  <  \epsilon_j $  for $i < j$, allows us  to define the  trial wave function.  In particular, for the half-filled case, we set 
\begin{lstlisting}[style=fortran,escapechar=\#]
Do s = 1, N_fl
   Do x = 1,Ndim
      Do n = 1, N_part
         WF_L(s)%P(x,n)  = # $U_{x,n}$ #
         WF_R(s)%P(x,n)  = # $U_{x,n}$ #
      Enddo
   Enddo
Enddo
\end{lstlisting}
with \texttt{N\_part = Ndim/2}.     The  variable \texttt{Degen}   belonging to the \texttt{WaveFunction}  type  is given by  \texttt{Degen}$=\epsilon_{N_{\mathrm{Part}} +1 } - \epsilon_{N_{\mathrm{Part}}  }$.   This quantity should be greater than zero  for non-degenerate trial wave functions. 

%------------------------------------------------------------
\subsection{Observables}
%------------------------------------------------------------

At this point, all the information for starting the simulation has been provided.  The code will sequentially go through  the operator list  \texttt{Op\_V}  and update the  fields.  Between  time slices  \texttt{LOBS\_ST}  and  \texttt{LOBS\_EN} the main program will call the routine  \texttt{Obser(GR,Phase,Ntau)}, which handles equal-time correlation functions, and, if \texttt{Ltau=1}, the routine \texttt{ObserT(NT,  GT0,G0T,G00,GTT, PHASE)} which handles imaginary-time displaced correlation functions. 

Both \texttt{Obser} and \texttt{ObserT} should be provided by the user, who can either implement themselves the observables they want to compute or use the predefined structures of Chap.~\ref{Predefined_chap}. Here we describe how to proceed in order to define an observable. 

%------------------------------------------------------------------------------------
\subsubsection[Allocating space for the observables: \texttt{Alloc\_obs}]{Allocating space for the observables: \texttt{Alloc\_obs(Ltau)}} \label{Alloc_obs_sec}
%-------------------------------------------------------------------------------------

For  four scalar  or vector observables,  the user will have to  declare the following: 
\begin{lstlisting}[style=fortran]
Allocate ( Obs_scal(4) )
Do I = 1,Size(Obs_scal,1)
   select case (I)
   case (1)
      N = 2;  Filename ="Kin"
   case (2)
      N = 1;  Filename ="Pot"
   case (3)
      N = 1;  Filename ="Part"
   case (4)
      N = 1,  Filename ="Ener"
   case default
      Write(6,*) ' Error in Alloc_obs '  
   end select
   Call Obser_Vec_make(Obs_scal(I), N, Filename)
enddo
\end{lstlisting}
Here,   \texttt{Obs\_scal(1)}   contains a vector  of two observables  so as to account for the $x$- and $y$-components of the kinetic energy, for example.  

For equal-time correlation functions  we allocate  \texttt{Obs\_eq}  of type \texttt{Obser\_Latt}.  Here we include the calculation of spin-spin and density-density correlation functions alongside equal-time Green functions. 
\begin{lstlisting}[style=fortran]
Allocate ( Obs_eq(5) )
Do I = 1,Size(Obs_eq,1)
   select case (I)
   case (1)
      Filename = "Green"
   case (2)
      Filename = "SpinZ"
   case (3)
      Filename = "SpinXY"
   case (4)
      Filename = "SpinT"
   case (5)
      Filename = "Den"
   case default
      Write(6,*) "Error in Alloc_obs"
   end select
   Nt = 1
   Channel = "--"
   Call Obser_Latt_make(Obs_eq(I), Nt, Filename, Latt, Latt_unit, Channel, dtau)
Enddo
\end{lstlisting} 
Be aware that \texttt{Obser\_Latt\_make} does not copy the Bravais lattice \texttt{Latt} 
and unit cell \texttt{Latt\_unit}, but links them through pointers to be more memory 
efficient. One can have different lattices attached to different observables by declaring 
additional instances of \texttt{Type(Lattice)} and \texttt{Type(Unit\_cell)}.
 For equal-time correlation functions, we set \texttt{Nt = 1} and \texttt{Channel} specification is not necessary.

If \texttt{Ltau = 1}, then the code allocates space for time displaced quantities. The 
same structure as for equal-time correlation functions is used, albeit with 
\texttt{Nt = Ltrot + 1} and the channel should be set. Whith \texttt{Channel="PH"}, for instance, the analysis algorithm assumes the observable to be particle-hole symmetric. For more details on this parameter, see Sec.~\ref{sec:maxent}.

At the beginning of each bin, the main program will set the bin observables to zero by calling  the routine \texttt{Init\_obs(Ltau)}. The user does not have to edit this routine. 
 
%-------------------------------------------------------------------------------------
\subsubsection[Measuring equal-time observables: \texttt{Obser}]{Measuring equal-time observables: \texttt{Obser(GR,Phase,Ntau)}} \label{sec:EqualTimeobs}
%-------------------------------------------------------------------------------------

Having allocated the necessary memory, we proceed to define the observables. The equal-time  Green function,
\begin{equation}
	 \texttt{GR(x,y},\sigma{\texttt)}  = \langle \hat{c}^{\phantom{\dagger}}_{x,\sigma} \hat{c}^{\dagger}_{y,\sigma}  \rangle,
\end{equation}
the  phase factor \texttt{phase} [Eq.~(\ref{eqn:phase})], and time slice \texttt{Ntau}   are provided by the main program.  

Here,   $x$ and $y$ label  both unit cell as well as the orbital within the unit cell. For the Hubbard model described here, $x$ corresponds to the unit cell.  The Green function  does not depend on the color index, and is diagonal in flavor.  For the SU(2) symmetric implementation  there is only one flavor, $\sigma = 1$ and the Green function is  independent on the spin index.  This renders the calculation of the observables particularly easy.   

An explicit calculation of the   potential energy  $ \langle U \sum_{\vec{i}}  \hat{n}_{\vec{i},\uparrow}   \hat{n}_{\vec{i},\downarrow}  \rangle $ reads 
\begin{lstlisting}[style=fortran]
Obs_scal(2)%N        = Obs_scal(2)%N + 1
Obs_scal(2)%Ave_sign = Obs_scal(2)%Ave_sign + Real(ZS,kind(0.d0))
Do i = 1,Ndim
  Obs_scal(2)%Obs_vec(1)= Obs_scal(2)%Obs_vec(1) +(1-GR(i,i,1))*(1-GR(i,i,2))*Ham_U*ZS*ZP
Enddo
\end{lstlisting} 
Here  $ \texttt{ZS} = \sgn(C) $  [see Eq.~(\ref{Sign.eq})],  $ \texttt{ZP} =   \frac{e^{-S(C)}} {\Re \left[e^{-S(C)} \right]}   $ [see Eq.~(\ref{eqn:phase})] and  \texttt{Ham\_U}  corresponds to the Hubbard  $U$ term.

Equal-time correlations  are also computed in this routine. As an explicit example, we  consider the equal-time density-density correlation:
\begin{equation}
	 \langle \hat{n}_{\vec{i}}   \hat{n}_{\vec{j}} \rangle   -  \langle \hat{n}_{\ve{i} }\rangle  \langle    \hat{n}_{\ve{j}}  \rangle,
\end{equation} 
with
\begin{equation}
	 \hat{n}_{\vec{i}}  =   \sum_{\sigma} \hat{c}^{\dagger}_{\ve{i},\sigma} \hat{c}^{\phantom\dagger}_{\ve{i},\sigma}.
\end{equation}
For the calculation of such quantities, it is convenient to  define: 
\begin{equation}
\label{GRC.eq}
	\texttt{GRC(x,y,s)}   =  \delta_{x,y}  - \texttt{GR(y,x,s)  }
\end{equation}
such that \texttt{GRC(x,y,s)}    corresponds to  $ \langle \langle  \hat{c}_{x,s}^{\dagger}\hat{c}_{y,s}^{\phantom\dagger} \rangle \rangle $. 
In the program code, the calculation of the equal-time density-density correlation function looks as follows:
\begin{lstlisting}[style=fortran]
Obs_eq(4)%N = Obs_eq(4)%N + 1           ! Even if it is redundant, each observable  
                                        ! carries its own counter and sign.
Obs_eq(4)%Ave_sign = Obs_eq(4)%Ave_sign + Real(ZS,kind(0.d0))  
Do I = 1,Ndim
   Do J = 1,Ndim                       
      imj = latt%imj(I,J)
      Obs_eq(4)%Obs_Latt(imj,1,1,1) =  Obs_eq(4)%Obs_Latt(imj,1,1,1) + &
                     &     ( (GRC(I,I,1)+GRC(I,I,2)) * (GRC(J,J,1)+GRC(J,J,2))       + &
                     &        GRC(I,J,1)*GR(I,J,1)   +  GRC(I,J,2)*GR(I,J,2)  ) * ZP * ZS 
   Enddo
   Obs_eq(4)%Obs_Latt0(1) = Obs_eq(4)%Obs_Latt0(1) + (GRC(I,I,1)+GRC(I,I,2))*ZP*ZS
Enddo
\end{lstlisting} 
At the end of each bin the main program calls the routine \texttt{ Pr\_obs(LTAU)}. This routine appends the result for the current bins to the corresponding file, with the appropriate suffix. 

%-------------------------------------------------------------------------------------
\subsubsection[Measuring time-displaced observables: \texttt{ObserT}]{Measuring time-displaced observables: \texttt{ObserT(NT, GT0, G0T, G00, GTT, PHASE)}}  \label{sec:TimeDispObs}
%-------------------------------------------------------------------------------------
%
This subroutine is called by the main program at the beginning of each sweep, provided that \texttt{LTAU}  is set to $1$. The variable \texttt{NT} runs from \texttt{0}  to \texttt{Ltrot} and denotes the imaginary time difference. For a given time  displacement, the main program provides:
\begin{align}
\begin{aligned}
\label{Time_displaced_green.eq}
\texttt{GT0(x,y,s) }  &=   \phantom{+} \langle \langle \hat{c}^{\phantom\dagger}_{x,s} (Nt \Delta \tau)   \hat{c}^{\dagger}_{y,s} (0)   \rangle \rangle \;=\; \langle \langle \mathcal{T} \hat{c}^{\phantom\dagger}_{x,s} (Nt \Delta \tau)   \hat{c}^{\dagger}_{y,s} (0)   \rangle \rangle   \\
\texttt{G0T(x,y,s) }   &=  -   \langle \langle   \hat{c}^{\dagger}_{y,s} (Nt \Delta \tau)    \hat{c}^{\phantom\dagger}_{x,s} (0)    \rangle \rangle \;=\;
    \langle \langle \mathcal{T} \hat{c}^{\phantom\dagger}_{x,s} (0)    \hat{c}^{\dagger}_{y,s} (Nt \Delta \tau)   \rangle \rangle    \\
  \texttt{G00(x,y,s) }  &=    \phantom{+} \langle \langle \hat{c}^{\phantom\dagger}_{x,s} (0)   \hat{c}^{\dagger}_{y,s} (0)   \rangle \rangle     \\
    \texttt{GTT(x,y,s) }  &=   \phantom{+} \langle \langle \hat{c}^{\phantom\dagger}_{x,s} (Nt \Delta \tau)   \hat{c}^{\dagger}_{y,s} (Nt \Delta \tau)   \rangle \rangle.
\end{aligned}
\end{align}
In the above we have omitted the color index since  the  Green functions are color independent.  The time-displaced spin-spin correlations 
$ 4 \langle \langle \hat{S}^{z}_{\vec{i}} (\tau)  \hat{S}^{z}_{\vec{j}} (0)\rangle \rangle   $ 
are then given by: 
\begin{multline}
	4 \langle \langle \hat{S}^{z}_{\vec{i}} (\tau)  \hat{S}^{z}_{\vec{j}} (0)\rangle \rangle
	=  ( \texttt{GTT(I,I,1)} -  \texttt{GTT(I,I,2)} ) * ( \texttt{G00(J,J,1)} -  \texttt{G00(J,J,2)} )     \\  
	-   \; \texttt{G0T(J,I,1)}*\texttt{GT0(I,J,1)}  -  \texttt{G0T(J,I,2)}* \texttt{GT0(I,J,2)}
\end{multline}

The handling of time-displaced correlation functions is identical to that of equal-time correlations. 

%-------------------------------------------------------------------------------------
\subsection{Numerical precision}\label{sec:prec_spin}
%-------------------------------------------------------------------------------------

Information on the numerical stability is included in the following lines of the corresponding file \texttt{info}. 
For a  \textit{short} simulation on a $4 \times 4$  lattice at $U/t=4$ and $\beta t = 10$  we obtain
\begin{lstlisting}[basicstyle=\ttfamily\small,columns=fullflexible,keepspaces=true]
Precision Green  Mean, Max :   5.0823874429126405E-011  5.8621144596315844E-006
Precision Phase  Max       :   0.0000000000000000    
Precision tau    Mean, Max :   1.5929357848647394E-011  1.0985132530727526E-005 
\end{lstlisting}
showing the mean and maximum difference between the \textit{wrapped}  and from scratched computed equal and time-displaced  Green functions \cite{Assaad08_rev}.
A stable code  should produce results where the mean difference is smaller than the  stochastic error. The above example  shows a very stable simulation since the Green function  is of order one. 

\subsection{Running the code and testing}

To test the code, one can carry out high precision simulations. After compilation, the executable \texttt{Hubbard\_Plain\_Vanilla.out} is found in the directory \texttt{\$ALF\_DIR/Prog/} and can be run from any directory containing the files \texttt{parameters} and \texttt{seeds} (See Sec.~\ref{sec:files}).

Alternatively, as we do bellow, it may be convenient to use \texttt{pyALF} to compile and run the code, especially when using one of the scripts or notebooks available.  

\paragraph*{One-dimensional case} 

The \texttt{pyALF} python script   \href{https://git.physik.uni-wuerzburg.de/ALF/pyALF/-/blob/ALF-2.0/Scripts/Hubbard_Plain_Vanilla.py}{\texttt{Hubbard\_Plain\_Vanilla.py}}   runs the projective version of the code for the four-site Hubbard model.  At $\theta t =10$, $\Delta \tau t = 0.05 $ with the symmetric Trotter  decomposition, we obtain after 40 bins of 2000 sweeps each the total energy:   
\begin{equation*}
       \langle  \hat{H}   \rangle = -2.103750  \pm      0.004825,
 \end{equation*}
and the exact result is  
\begin{equation*}
\langle  \hat{H}   \rangle_{\texttt{Exact}}    = -2.100396.
\end{equation*}

\paragraph*{Two-dimensional case}  
For the two-dimensional case,   with similar parameters, we obtain the results listed in Table~\ref{tab:2dplain}.
\begin{table}[h!]
\begin{center}
\begin{tabular}{l l l}
\toprule
             &  QMC  & Exact  \\ \midrule
Total energy & -13.618   $\pm $  0.002 &  -13.6224  \\
 $\ve{Q}=(\pi,\pi)$ spin correlations &  \phantom{-1}3.630     $ \pm $   0.006     & \phantom{-1}3.64 \\ 
 \bottomrule
\end{tabular}
\caption{Test results for the \texttt{Hubbard\_Plain\_Vanilla} code on a two-dimensional lattice with default parameters.} \label{tab:2dplain}
\end{center}
\end{table}
The exact results stem from Ref.~\cite{Parola91}     and the slight discrepancies from the exact results can be  assigned to the finite value of $\Delta \tau$.  Note that all the simulations were carried out with the default value of the Hubbard interaction, $U/t =4$. 
