% Copyright (c) 2016, 2017 The ALF project.
% This is a part of the ALF project documentation.
% The ALF project documentation by the ALF contributors is licensed
% under a Creative Commons Attribution-ShareAlike 4.0 International License.
% For the licensing details of the documentation see license.CCBYSA.

% !TEX root = doc.tex

%\red{Summarize and change slightly -- or completely omit -- in order to reduce overlap with ALF 1.0 paper (unless this paper is to be published as a 2nd version of the first - especially since we'd prefer to have it self-contained).}//


We start this section by deriving the detailed form of the partition function and outlining the computation of observables (Sec.~\ref{sec:part} - \ref{sec:reweight}). 
Next, we present a number of update strategies, namely local updates, global updates,  parallel tempering  and Langevin dynamics  (Sec.~\ref{sec:updating}). 
We then discuss the Trotter error, both for symmetric and asymmetric decompositions (Sec.~\ref{sec:trotter}) and, finally, we describe the measures we have implemented to make the code numerically stable (Sec.~\ref{sec:stable}).
%Finally, we discuss the autocorrelations and associated time scales during the Monte Carlo sampling process (Sec.~\ref{sec:sampling}). 

%------------------------------------------------------------
\subsection{Formulation of the method}  \label{sec:method}
%------------------------------------------------------------
Our aim is to compute observables  for the general Hamiltonian  (\ref{eqn:general_ham}) in
 thermodynamic equilibrium as described by the grand-canonical ensemble.
We show below  how the grand-canonical partition function can be rewritten as 
\begin{equation}
Z = \Tr{\left(e^{-\beta \hat{\mathcal{H}} }\right)}
= \sum_{C} e^{-S(C) } + \mathcal{O}(\Delta\tau^{2}),
\end{equation}
and define the space of configurations  $C$. Note that the chemical potential term is already included in the definition of the one-body term ${\mathcal{\hat{H}}_{T}}$, see Eq. \eqref{eqn:general_ham_t}, of the general Hamiltonian.  
The essential ingredients of the auxiliary-field quantum Monte Carlo implementation in the ALF package are the following:
\begin{itemize}
\item  We discretize the imaginary time propagation: $\beta = \Delta \tau L_{\text{Trotter}} $. Generically this introduces a systematic Trotter error of $\mathcal{O}(\Delta \tau)^2$  \cite{Fye86}. 
We note that there has been considerable effort at getting rid of the Trotter systematic error and to formulate a genuine continuous-time BSS  algorithm \cite{Iazzi15}.   To date, efforts in this direction that are based on a CT-AUX type formulation \cite{Rombouts99,Gull08} face two issues. The first one is that they are restricted to a class of models with Hubbard-type interactions
\begin{align}
        (\hat{n}_{i}- 1)^{2}  = (\hat{n}_{i}- 1)^{4} ,
\end{align}
in order for the basic CT-AUX equation \cite{Rombouts98},
\begin{equation}
          1   + \frac{U}{K} \left(  \hat{n}_{i}- 1\right)^{2}    = \frac{1}{2}\sum_{s=\pm 1}   e^{ \alpha s \left(  \hat{n}_{i}- 1\right) }  \; \text{ with  }  \;  \frac{U}{K} = \cosh(\alpha) -1 \; \text{ and  }  \; K\in\mathbb{R},
\end{equation}
to hold.
The second issue is that it is hard to formulate a  computationally efficient algorithm.  Given this situation, if eliminating the Trotter systematic error is required, it turns out that extrapolating to small imaginary-time steps using the multi-grid method \cite{Rost12,Rost13,Bluemer08} is a more efficient scheme.

There has also been progress in efficient continuous-time methods using techniques that draw from the Stochastic Series Expansion \cite{Wang16} which can be combined with fermion bag ideas \cite{Huffman17}. However, these techniques are even more restricted to a specific class of Hamiltonians, those that can be expressed as sums of exponentiated fermionic bilinear terms $\hat{H} = \sum_i \hat{h}^{(i)}$, where
\begin{equation}
\hat{h}^{(i)} = -\gamma^{(i)} e^{\sum_{jk} \alpha^{(i)}_{jk} \hat{c}^\dagger_j  \hat{c}_k + \hc }
\end{equation}
Stabilization can also be costly depending on the parameters, particularly for large $\alpha$ values \cite{huffman2019}.
\item  Having isolated the two-body term, we apply Gau\ss{}-Hermite quadrature\footnote{We would like to thank Kazuhiro Seki for discussions on this subject.} \cite{goth2020} to the 
continuous HS transform and obtain the discrete HS transformation \cite{Motome97,Assaad97}:
\begin{equation}
\label{HS_squares}
        e^{\Delta \tau  \lambda  \hat{A}^2 } = \frac{1}{4}
        \sum_{ l = \pm 1, \pm 2}  \gamma(l)
e^{ \sqrt{\Delta \tau \lambda }
       \eta(l)  \hat{A} }
                + \mathcal{O} \left(  (\Delta \tau \lambda)^4\right) \;,
\end{equation}
where the fields $\eta$ and $\gamma$ take the values:
\begin{equation} \label{eta_gamma_fields}
\begin{aligned}
\gamma(\pm 1) &= 1 + \sqrt{6}/3, \quad & \eta(\pm 1 ) &= \pm \sqrt{2 \left(3 - \sqrt{6} \right)},\\
\gamma(\pm 2) &= 1 - \sqrt{6}/3, \quad & \eta(\pm 2 ) &= \pm \sqrt{2 \left(3 + \sqrt{6} \right)}.
\end{aligned}
\end{equation}
Since the Trotter error is already of order $(\Delta \tau ^2) $ per time slice, this transformation is next to exact.
One can relate the expectation value of the field $\eta(l)$ to the operator $\hat{A}$  by noting that:
\begin{eqnarray} 
    & &  \frac{1}{4} \sum_{l = \pm 1, \pm 2} \gamma(l) e^{\sqrt{ \Delta \tau \lambda} \eta(l) \hat{A}} \left( \frac{\eta(l)}{-2 \sqrt{ \Delta \tau \lambda}  } \right)    =   e^{ \Delta \tau \lambda  \hat{A}^2}  \hat{A}  +   \mathcal{O}  \left( (\Delta \tau \lambda)^3\right)   \text{  and } \nonumber \\  
     & &    \frac{1}{4} \sum_{l = \pm 1, \pm 2} \gamma(l) e^{\sqrt{ \Delta \tau \lambda} \eta(l) \hat{A}} \left( \frac{(\eta(l))^2 - 2}{4 \Delta \tau \lambda  } \right)    =   e^{ \Delta \tau \lambda  \hat{A}^2}  \hat{A}^2  +   \mathcal{O}  \left( (\Delta \tau \lambda)^2\right).
\end{eqnarray}
\item $\hat{Z}_k$ in Eq.~\eqref{eqn:general_ham_i} can stand for a variety of operators, such as the Pauli matrix $\hat{\sigma}_{z}$ -- in which case the Ising spins take the values $s_{k} = \pm 1$ -- or the position operator -- such that $ \hat{Z}_k | \phi\rangle = \phi_k  |\phi\rangle $, with $\phi_k$ a real number.  

\item From the above it follows that the  Monte Carlo configuration space $C$  
is given by the combined spaces of bosonic configurations  and of HS discrete field configurations:
\begin{equation}
	C = \left\{   \phi_{i,\tau} ,  l_{j,\tau}  \text{ with }  i=1\cdots M_I,\;  j = 1\cdots M_V,\; \tau=1\cdots L_{\mathrm{Trotter}}  \right\}.
\end{equation}
Here, the HS fields take the values $l_{j,\tau} = \pm 2, \pm 1 $ and  $\phi_{i,\tau}$ may, for instance, be a continuous real field or, if $\hat{Z}_k = \hat{\sigma}_{z}$, be restricted to $\pm 1$.
\end{itemize}

%------------------------------------------------------------
\subsubsection{The partition function}\label{sec:part}
%------------------------------------------------------------

With the above, the partition function of the model (\ref{eqn:general_ham}) can be written as follows.
\begin{align}\label{eqn:partition_1}
Z &= \Tr{\left(e^{-\beta \hat{\mathcal{H}} }\right)}\nonumber\\
  &=   \Tr{  \left[ e^{-\Delta \tau \hat{\mathcal{H}}_{0,I}}  
    \prod_{k=1}^{M_V}   e^{ - \Delta \tau  U_k \left(  \hat{V}^{(k)} \right)^2}   \prod_{k=1}^{M_I}   e^{  -\Delta \tau  \hat{\sigma}_{k}  \hat{I}^{(k)}} 
     \prod_{k=1}^{M_T}   e^{-\Delta \tau \hat{T}^{(k)}}  
   \right]^{L_{\text{Trotter}}}}  + \mathcal{O}(\Delta\tau^{2})\nonumber \\
   &=
   \sum_{C} \left( \prod_{k=1}^{M_V} \prod_{\tau=1}^{L_{\mathrm{Trotter}}} \gamma_{k,\tau} \right) e^{-S_0 \left( \left\{ s_{i,\tau} \right\}  \right) }\times \nonumber\\
   &\quad
    \Trf{ \left\{  \prod_{\tau=1}^{L_{\mathrm{Trotter}}} \left[  
    \prod_{k=1}^{M_V}   e^{  \sqrt{ -\Delta \tau  U_k} \eta_{k,\tau} \hat{V}^{(k)} }   \prod_{k=1}^{M_I}   e^{  -\Delta \tau s_{k,\tau}  \hat{I}^{(k)}}  
      \prod_{k=1}^{M_T}   e^{-\Delta \tau \hat{T}^{(k)}}    \right]\right\} }+ \mathcal{O}(\Delta\tau^{2})\;.
\end{align}
In the above,  the trace $\mathrm{Tr} $  runs over the bosonic   and  fermionic degrees of freedom, and $ \mathrm{Tr}_{\mathrm{F}}  $ only over the  fermionic Fock space. 
$S_0 \left( \left\{ s_{i,\tau} \right\}  \right)  $ is the action  corresponding to the bosonic Hamiltonian, and is only dependent on the bosonic fields so that  it can be pulled out of the fermionic trace.  We have adopted the shorthand notation $\eta_{k,\tau} \equiv \eta(l_{k,\tau})$   and $\gamma_{k,\tau} \equiv \gamma(l_{k,\tau})$.
At this point,  and  since for a given configuration $C$  we are dealing with a free propagation, we can integrate out the fermions to obtain a determinant: 
\begin{multline}
\Trf{ \left\{  \prod_{\tau=1}^{L_{\mathrm{Trotter}}} \left[    
    \prod_{k=1}^{M_V}   e^{  \sqrt{ - \Delta \tau  U_k} \eta_{k,\tau} \hat{V}^{(k)} }   \prod_{k=1}^{M_I}   e^{  -\Delta \tau s_{k,\tau}  \hat{I}^{(k)}} 
    \prod_{k=1}^{M_T}   e^{-\Delta \tau \hat{T}^{(k)}}   \right]   \right\}} = \\
\begin{aligned}
&\prod\limits_{s=1}^{N_{\mathrm{fl}}} \left[  e^{\sum\limits_{k=1}^{M_V} \sum\limits_{\tau = 1}^{L_{\mathrm{Trotter}}}\sqrt{-\Delta \tau U_k}  \alpha_{k,s} \eta_{k,\tau} } \right]^{N_{\mathrm{col}}} \times \\
&\prod\limits_{s=1}^{N_{\mathrm{fl}}} 
   \left[
    \det\left(  \mathds{1} + 
     \prod_{\tau=1}^{L_{\mathrm{Trotter}}}   
    \prod_{k=1}^{M_V}   e^{  \sqrt{ -\Delta \tau  U_k} \eta_{k,\tau} {\bm V}^{(ks)} }   \prod_{k=1}^{M_I}   e^{  -\Delta \tau s_{k,\tau}  {\bm I}^{(ks)}}  
      \prod_{k=1}^{M_T}   e^{-\Delta \tau \bm{T}^{(ks)}}   \right) \right]^{N_{\mathrm{col}}},
\end{aligned}
\end{multline}
where the matrices $\bm{T}^{(ks)}$,  $\bm{V}^{(ks)}$, and  $\bm{I}^{(ks)}$ define the Hamiltonian [Eq.~(\ref{eqn:general_ham}) - (\ref{eqn:general_ham_i})].
All in all, the partition function is given by:
\begin{align}\label{eqn:partition_2}
    Z  &=   \begin{multlined}[t]   \sum_{C}   e^{-S_0 \left( \left\{ s_{i,\tau} \right\}  \right) }     \left( \prod_{k=1}^{M_V} \prod_{\tau=1}^{L_{\mathrm{Trotter}}} \gamma_{k,\tau} \right)
    e^{ N_{\mathrm{col}}\sum\limits_{s=1}^{N_{\mathrm{fl}}} \sum\limits_{k=1}^{M_V} \sum\limits_{\tau = 1}^{L_{\mathrm{Trotter}}}\sqrt{-\Delta \tau U_k}  \alpha_{k,s} \eta_{k,\tau} } 
  \times  \prod_{s=1}^{N_{\mathrm{fl}}}\left[\det\left(  \mathds{1}   \phantom{\prod_{k=1}^{M_V}}   \right. \right. \\
   \left. \left.   + \prod_{\tau=1}^{L_{\mathrm{Trotter}}}   
    \prod_{k=1}^{M_V}   e^{  \sqrt{ -\Delta \tau  U_k} \eta_{k,\tau} {\bm V}^{(ks)} }   \prod_{k=1}^{M_I}   e^{  -\Delta \tau s_{k,\tau}  {\bm I}^{(ks)}}  
     \prod_{k=1}^{M_T}   e^{-\Delta \tau {\bm T}^{(ks)}} 
     \right) \right]^{N_{\mathrm{col}}} + \mathcal{O}(\Delta\tau^{2})  \end{multlined} \nonumber \\ 
     & \equiv  \sum_{C} e^{-S(C) } + \mathcal{O}(\Delta\tau^{2})\;.
\end{align}
In the above, one notices that the weight factorizes in  the flavor index. The color index raises the determinant to the power $N_{\mathrm{col}}$. 
This corresponds to  an explicit SU($N_{\mathrm{col}}$) symmetry   for each  configuration. This symmetry is manifest in the fact that the single particle  Green functions are color independent, again for each given  configuration $C$.

%------------------------------------------------------------
\subsubsection{Observables}\label{sec:Observables.General}
%------------------------------------------------------------

In the auxiliary-field QMC approach, the single-particle Green function plays a crucial role.  It determines the Monte Carlo dynamics and is used to compute  observables. Consider the observable:
\begin{equation}\label{eqn:obs}
\langle \hat{O}  \rangle  = \frac{ \text{Tr}   \left[ e^{- \beta \hat{H}}  \hat{O}  \right] }{ \text{Tr}   \left[ e^{- \beta \hat{H}}  \right] } =   \sum_{C}   P(C) 
   \langle \langle \hat{O}  \rangle \rangle_{(C)} ,
   \; \text{ where } 
  P(C)   = \frac{ e^{-S(C)}}{\sum_C e^{-S(C)}}\;
\end{equation}
and $\langle \langle \hat{O}  \rangle \rangle_{(C)} $ denotes the observed value of $\hat{O}$ for a given configuration $C$.
For a given configuration $C$  one can use Wick's theorem to compute $O (C) $   from the knowledge of the single-particle Green function: 
\begin{equation}
       G( x,\sigma,s, \tau |    x',\sigma',s', \tau')   =       \langle \langle \mathcal{T} \hat{c}^{\phantom\dagger}_{x \sigma s} (\tau)  \hat{c}^{\dagger}_{x' \sigma' s'} (\tau') \rangle \rangle_{C},
\end{equation}
where $ \mathcal{T} $ denotes the imaginary-time ordering operator.   The  corresponding equal-time quantity reads
\begin{equation}
       G( x,\sigma,s, \tau |    x',\sigma',s', \tau)   =       \langle \langle  \hat{c}^{\phantom\dagger}_{x \sigma s} (\tau)  \hat{c}^{\dagger}_{x' \sigma' s'} (\tau) \rangle \rangle_{C}.
\end{equation}
Since, for a given HS field, translation invariance in imaginary-time is broken, the Green function has an explicit $\tau$ and $\tau'$ dependence.   On the other hand it is diagonal in the flavor index, and independent of the color index. The latter reflects the  explicit SU(N) color symmetry present at the level of individual HS configurations.   As an example,  one can show that the equal-time Green function at $\tau = 0$ reads \cite{Assaad08_rev}:
\begin{equation}\label{eqn:Green_eq}
G(x,\sigma,s,0| x',\sigma,s,0 )  =   \left(  \mathds{1}  +  \prod_{\tau = 1}^{L_{\text{Trotter}}}  \bm{B}_{\tau}^{(s)}   \right)^{-1}_{x,x'}
\end{equation}
with
\begin{equation}
\label{Btau.eq}
	\bm{B}_{\tau}^{(s)} =   
    \prod_{k=1}^{M_V}   e^{  \sqrt{ -\Delta \tau  U_k} \eta_{k,\tau} {\bm V}^{(ks)} }   \prod_{k=1}^{M_I}   e^{  -\Delta \tau s_{k,\tau}  {\bm I}^{(ks)}}
    \prod_{k=1}^{M_T}   e^{-\Delta \tau {\bm T}^{(ks)}} .
\end{equation}


To compute equal-time, as well as time-displaced observables, one can make use of Wick's theorem. A convenient formulation of this theorem for QMC simulations reads: 
\begin{multline}
\label{Wick.eq}
\langle \langle 	\mathcal{T}   \hat{c}^{\dagger}_{\underline x_{1}}(\tau_{1}) \hat{c}^{\phantom\dagger}_{{\underline x}'_{1}}(\tau'_{1})  
\cdots \hat{c}^{\dagger}_{\underline x_{n}}(\tau_{n}) \hat{c}^{\phantom\dagger}_{{\underline x}'_{n}}(\tau'_{n}) 
\rangle \rangle_{C} = \\
\det  
\begin{bmatrix}
   \langle \langle   \mathcal{T}   \hat{c}^{\dagger}_{\underline x_{1}}(\tau_{1}) \hat{c}^{\phantom\dagger}_{{\underline x}'_{1}}(\tau'_{1})  \rangle \rangle_{C} & 
    \langle \langle  \mathcal{T}   \hat{c}^{\dagger}_{\underline x_{1}}(\tau_{1}) \hat{c}^{\phantom\dagger}_{{\underline x}'_{2}}(\tau'_{2})  \rangle \rangle_{C}  & \dots   &   
    \langle \langle   \mathcal{T}   \hat{c}^{\dagger}_{\underline x_{1}}(\tau_{1}) \hat{c}^{\phantom\dagger}_{{\underline x}'_{n}}(\tau'_{n})  \rangle \rangle_{C}  \\
    \langle \langle   \mathcal{T}   \hat{c}^{\dagger}_{\underline x_{2}}(\tau_{2}) \hat{c}^{\phantom\dagger}_{{\underline x}'_{1}}(\tau'_{1})  \rangle \rangle_{C}  & 
      \langle \langle   \mathcal{T}   \hat{c}^{\dagger}_{\underline x_{2}}(\tau_{2}) \hat{c}^{\phantom\dagger}_{{\underline x}'_{2}}(\tau'_{2})  \rangle \rangle_{C}  & \dots  &
       \langle \langle   \mathcal{T}   \hat{c}^{\dagger}_{\underline x_{2}}(\tau_{2}) \hat{c}^{\phantom\dagger}_{{\underline x}'_{n}}(\tau'_{n})  \rangle \rangle_{C}   \\
    \vdots & \vdots &  \ddots & \vdots \\
    \langle \langle   \mathcal{T}   \hat{c}^{\dagger}_{\underline x_{n}}(\tau_{n}) \hat{c}^{\phantom\dagger}_{{\underline x}'_{1}}(\tau'_{1})  \rangle \rangle_{C}   & 
     \langle \langle   \mathcal{T}   \hat{c}^{\dagger}_{\underline x_{n}}(\tau_{n}) \hat{c}^{\phantom\dagger}_{{\underline x}'_{2}}(\tau'_{2})  \rangle \rangle_{C}   & \dots  & 
     \langle \langle   \mathcal{T}   \hat{c}^{\dagger}_{\underline x_{n}}(\tau_{n}) \hat{c}^{\phantom\dagger}_{{\underline x}'_{n}}(\tau'_{n})  \rangle \rangle_{C}
 \end{bmatrix}.
\end{multline}
Here, we have defined the super-index $\underline{ x} = \left\{   x,\sigma,s \right\}$.

Wick's theorem can be also used to express a reduced density matrix, i.e., the density matrix for a subsystem, in terms of its correlations \cite{Peschel03}.
Within the framework of Auxiliary-Field QMC, this allows to express a reduced density matrix $\hat{\rho}_A$ for a subsystem $A$ as \cite{Grover13}
\begin{equation}
\hat{\rho}_A = \sum_C P(C){\rm det}(\mathds{1}-G_A(\tau_0; C))e^{-c^{\dagger}_{\underline x} H^{(A)}_{{\underline x}, {\underline x}'}c^{\phantom\dagger}_{{\underline x}'}},\qquad H^{(A)} \equiv \ln\left\{\left[\left(G_A(\tau_0; C)\right)^T\right]^{-1}-\mathds{1}\right\},
\label{rhoA}
\end{equation}
where $G_A(\tau_0; C)$ is the equal-time Green's function matrix restricted on the subsystem $A$ and at a given time-slice $\tau_0$. In Eq.~(\ref{rhoA}) an implicit summation over repeated indexes ${\underline x}, {\underline x}' \in A$ is assumed.
Interestingly, Eq.~(\ref{rhoA}) holds also when $A$ is the entire system: in this case, it provides an alternative expression for the density matrix, or the (normalized) partition function, as a superposition of Gaussian operators.
Eq.~(\ref{rhoA}) is the starting point for computing the entanglement Hamiltonian \cite{Toldin18} and the R\'enyi entropies \cite{Grover13,Assaad13a,Assaad15}.
A short review on various computational approaches to quantum entanglement in interacting fermionic models can be found in Ref.~\cite{ParisenToldin19}.
ALF provides predefined observables to compute the second R\'enyi entropy and its associated mutual information, see Sec.~\ref{sec:renyi}.


% Removed to refer to predefined obs. instead: In the subroutines   \path{Obser}  and \path{ObserT} of  the module \path{Hamiltonian_Examples_mod.F90} (see Sec.~\ref{sec:obs}) the user is provided with
In Sec.~\ref{sec:predefined_observales} we describe the equal-time and time-displaced correlation functions that come predefined in ALF.
Using the  above  formulation  of  Wick's theorem, arbitrary  correlation functions can be computed (see Appendix \ref{Wick_appendix}). We note, however, that the program is limited to the calculation of observables that contain only two different imaginary times.  

%------------------------------------------------------------
\subsubsection{Reweighting and the sign problem}\label{sec:reweight}
%------------------------------------------------------------

In general, the action  $S(C) $ will be complex, thereby inhibiting a direct Monte Carlo sampling of $P(C)$.   This leads to the infamous sign problem.     The sign problem is formulation dependent and as noted above, much progress has been made at understanding the class of models that  can be formulated without encountering this problem 
\cite{Wu04,Huffman14,Yao14a,Wei16}.  When the average sign is not too small, we can nevertheless  compute observables within a reweighting scheme.   Here we adopt the following scheme. First  note  that the partition function is real such that: 
\begin{equation}
	Z =   \sum_{C}  e^{-S(C)}    =  \sum_{C}  \overline{e^{-S(C)}} = \sum_{C}  \Re \left[e^{-S(C)} \right]. 
\end{equation}
Thereby\footnote{The attentive reader will have noticed that   for arbitrary Trotter decompositions,  the  imaginary time propagator is not necessarily Hermitian. Thereby, the above equation is correct only up to corrections stemming from the  controlled Trotter systematic error. }
and with the definition
\begin{equation}
\label{Sign.eq}
	 \sgn(C)   =  \frac{   \Re \left[e^{-S(C)} \right]  } {\left| \Re \left[e^{-S(C)} \right]  \right|  }\;,
\end{equation}
the computation of the observable [Eq.~(\ref{eqn:obs})] is re-expressed as follows:
\begin{align}\label{eqn:obs_rw}
\langle \hat{O}  \rangle  &=  \frac{\sum_{C}  e^{-S(C)} \langle \langle \hat{O}  \rangle \rangle_{(C)} }{\sum_{C}  e^{-S(C)}}       \nonumber \\ 
                          &=  \frac{\sum_{C}   \Re \left[e^{-S(C)} \right]    \frac{e^{-S(C)}} {\Re \left[e^{-S(C)} \right]}  \langle \langle \hat{O}  \rangle \rangle_{(C)} }{\sum_{C}   \Re \left[e^{-S(C)} \right]}    \nonumber \\ 
          &=
   \frac{
     \left\{
      \sum_{C}  \left| \Re \left[e^{-S(C)} \right]  \right|   \sgn(C)   \frac{e^{-S(C)}} {\Re \left[e^{-S(C)} \right]}  \langle \langle \hat{O}  \rangle \rangle_{(C)}  \right\}/
            \sum_{C}  \left| \Re \left[ e^{-S(C)} \right] \right|  
          }  
          { 
          \left\{ \sum_{C}  \left|  \Re \left[ e^{-S(C)} \right]   \right|   \sgn(C) \right\}/
            \sum_{C}   \left| \Re \left[ e^{-S(C)} \right] \right|  
          } \nonumber\\
          &=
  	 \frac{  \left\langle  \sgn   \frac{e^{-S}} {\Re \left[e^{-S} \right]}  \langle \langle \hat{O}  \rangle \rangle  \right\rangle_{\overline{P}} } { \langle \sgn \rangle_{\overline{P}}}  \;.      
\end{align} 
The average sign is 
\begin{equation}\label{eqn:sign_rw}
	 \langle \sgn \rangle_{\overline{P}} =    \frac { \sum_{C}  \left|  \Re \left[ e^{-S(C)} \right]   \right|   \sgn(C) }  {  \sum_{C}   \left| \Re \left[ e^{-S(C)} \right] \right|  } \;,
\end{equation}
and we have  $\langle \sgn \rangle_{\overline{P}} \in \mathbb{R}$ per definition.
The Monte Carlo simulation samples the probability distribution 
\begin{equation}  
	 \overline{P}(C) = \frac{ \left|  \Re \left[ e^{-S(C)} \right] \right| }{\sum_C \left|  \Re \left[ e^{-S(C)} \right]  \right| }\;.
\label{Eq:Pbar}
\end{equation}
such that the nominator and denominator of  Eq.~(\ref{eqn:obs_rw})  can be computed.

Notice that, for the Langevin updating scheme with variable Langevin time step, a straightforward generalization of the equations above is used, see Sec.~\ref{sec:langevin}.

The negative sign problem is still an issue because the average sign is a ratio of two partition functions and one can argue that 
\begin{equation}
 \langle \sgn \rangle_{\overline{P}}   \propto e^{-  \Delta N \beta},
\end{equation}
where $\Delta $ is an intensive positive quantity and $N \beta$ denotes the  Euclidean volume.    In a Monte Carlo simulation the  error scales as $ 1/\sqrt{T_\text{CPU}} $   where $T_\text{CPU}$ corresponds to the computational  time.  Since the error on the  average sign has to be much smaller than the average sign itself,   one sees that:
\begin{equation}
	T_\text{CPU}  \gg e^{2 \Delta N \beta}.
\end{equation}   
Two comments are in order. First, the presence of a sign problem invariably leads to an exponential increase of CPU time as a function of the Euclidean volume. And second,  $\Delta$ is formulation dependent.  
For instance, at finite doping, the SU(2) invariant formulation of the Hubbard model presented in Sec.~\ref{sec:hubbard} has a much more severe sign problem than the formulation (presented in the same section) where the HS field couples to the $z$-component of the magnetization.   Optimization schemes minimize  $\Delta$  have been put forward in \cite{Wan20,Hangleiter20}.




 
