stages:
  - build
  - warnconv
  - test
  - memleaks

.exemptfiles_template: &exemptfiles_definition  # Hidden key that defines an anchor named 'exemptfiles_definition'
  except:
    changes:
      - .gitignore
      - CITATION
      - README.md
      - license.*

.build_template: &build_definition
  stage: build
  <<: *exemptfiles_definition
  script:
    - . configure.sh GNU serial
    - gfortran -v
    - make all

.build_template_pgi: &build_definition_pgi
  stage: build
  <<: *exemptfiles_definition
  script:
    - export PATH="/opt/pgi/linux86-64/2019/bin:$PATH"
    - pgfortran --version
    - . configure.sh PGI serial
    - make all

.warnconv_template: &warnconv_definition
  stage: warnconv
  <<: *exemptfiles_definition
  script:
    - . configure.sh Devel serial
    - gfortran -v
    - make all

.test_template: &test_definition
  stage: test
  <<: *exemptfiles_definition
  script:
    - . configure.sh Devel serial
    - gfortran -v
    - make lib
    - make ana
    - make Hubbard_Plain_Vanilla
    - cd testsuite
    - cmake -E make_directory tests
    - cd tests
    - cmake -G "Unix Makefiles" -DCMAKE_Fortran_FLAGS_RELEASE=${F90OPTFLAGS} -DCMAKE_BUILD_TYPE=RELEASE ..
    - cmake --build . --target all --config Release
    - ctest -VV -O log.txt
    - cat log.txt | grep "tests passed" | cut -d " " -f 1

GQMCT_Jessie:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/debian:jessie-gfortran-blas-lapack
  <<: *build_definition

GQMCT_Stretch:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/debian:stretch-gfortran-blas-lapack
  <<: *build_definition

GQMCT_Buster:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/debian:buster-gfortran-blas-lapack
  <<: *build_definition

GQMCT_Jessie_OpenBlas:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/debian:jessie-gfortran-openblas-lapack
  <<: *build_definition

GQMCT_Stretch_OpenBlas:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/debian:stretch-gfortran-openblas-lapack
  <<: *build_definition

GQMCT_CentOS:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi:centos-7-gfortran-blas-lapack
  <<: *build_definition

GQMCT_OpenSuse-13.2:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi:opensuse-13.2-gfortran-blas-lapack
  <<: *build_definition

GQMCT_OpenSuse-42.1:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi:opensuse-42.1-gfortran-blas-lapack
  <<: *build_definition

GQMCT_Buster-PGI:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/debian:buster-pgi1910
  <<: *build_definition_pgi

GQMCT_Ubuntu_Trusty:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/ubuntu:trusty-tahr-gfortran-lapack
  <<: *build_definition

GQMCT_Ubuntu_Xenial:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi:xenial-gfortran-lapack
  <<: *build_definition

GQMCT_Ubuntu_Bionic:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/ubuntu:bionic-beaver-gfortran-lapack
  <<: *build_definition

GQMCT_Stretch_MPI:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/debian:stretch-gfortran-blas-lapack-fftw-hdf5-scipy
  stage: build
  except:
    changes:
      - .gitignore
      - CITATION
      - README.md
      - license.*
  script:
    - gfortran -v
    - . configure.sh Devel Tempering
    - export ALF_FC="mpif90"
    - make all

GQMCT_Jessie_conv:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/debian:jessie-gfortran-blas-lapack
  <<: *warnconv_definition

GQMCT_Stretch_conv:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/debian:stretch-gfortran-blas-lapack
  <<: *warnconv_definition

GQMCT_Buster_conv:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/debian:buster-gfortran-blas-lapack
  <<: *warnconv_definition

GQMCT_Jessie_OpenBlas_conv:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/debian:jessie-gfortran-openblas-lapack
  <<: *warnconv_definition

GQMCT_Stretch_OpenBlas_conv:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/debian:stretch-gfortran-openblas-lapack
  <<: *warnconv_definition

GQMCT_CentOS_conv:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi:centos-7-gfortran-blas-lapack
  <<: *warnconv_definition

GQMCT_OpenSuse-13.2_conv:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi:opensuse-13.2-gfortran-blas-lapack
  <<: *warnconv_definition

GQMCT_OpenSuse-42.1_conv:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi:opensuse-42.1-gfortran-blas-lapack
  <<: *warnconv_definition

GQMCT_Buster-PGI_conv:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/debian:buster-pgi1910
  stage: warnconv
  except:
    changes:
      - .gitignore
      - CITATION
      - README.md
      - license.*
  script:
    - export PATH="/opt/pgi/linux86-64/2019/bin:$PATH"
    - pgfortran --version
    - . configure.sh PGI serial
    - make all

GQMCT_Jessie_tests:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/debian:jessie-gfortran-blas-lapack
  <<: *test_definition

GQMCT_Stretch_tests:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/debian:stretch-gfortran-blas-lapack
  <<: *test_definition

GQMCT_Buster_tests:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/debian:buster-gfortran-blas-lapack
  <<: *test_definition

GQMCT_Jessie_OpenBlas_tests:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/debian:jessie-gfortran-openblas-lapack
  <<: *test_definition

GQMCT_Stretch_OpenBlas_tests:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/debian:stretch-gfortran-openblas-lapack
  <<: *test_definition

GQMCT_CentOS_tests:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi:centos-7-gfortran-blas-lapack
  <<: *test_definition

GQMCT_OpenSuse-13.2_tests:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi:opensuse-13.2-gfortran-blas-lapack
  <<: *test_definition

GQMCT_OpenSuse-42.1_tests:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi:opensuse-42.1-gfortran-blas-lapack
  <<: *test_definition

GQMCT_Ubuntu-trusty_tests:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/ubuntu:trusty-tahr-gfortran-lapack
  <<: *test_definition

GQMCT_Ubuntu-bionic_tests:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/ubuntu:bionic-beaver-gfortran-lapack
  <<: *test_definition

GQMCT_Buster-PGI_tests:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/debian:buster-pgi1910-cmake
  stage: test
  except:
    changes:
      - .gitignore
      - CITATION
      - README.md
      - license.*
  script:
    - export PATH="/opt/pgi/linux86-64/2019/bin:$PATH"
    - pgfortran --version
    - . configure.sh PGI serial
    - make lib
    - make ana
    - make Hubbard_Plain_Vanilla
    - cd testsuite
    - cmake -E make_directory tests
    - cd tests
    - cmake -G "Unix Makefiles" -DCMAKE_Fortran_FLAGS_RELEASE="${F90OPTFLAGS} -noswitcherror -L/opt/pgi/linux86-64/2019/lib/ -llapack -lblas" -DCMAKE_Fortran_COMPILER="/opt/pgi/linux86-64/2019/bin/pgfortran" -DCMAKE_BUILD_TYPE=RELEASE -DLAPACK_LIBRARIES="/opt/pgi/linux86-64/2019/lib/liblapack.a" -DBLAS_LIBRARIES="/opt/pgi/linux86-64/2019/lib/libblas.a" ..
    - cmake --build . --target all --config Release
    - ctest -VV -O log.txt
    - cat log.txt | grep "tests passed" | cut -d " " -f 1

GQMCT_Stretch_valgrind:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/debian:stretch-gfortran-blas-lapack-valgrind
  stage: memleaks
  only:
    changes:
      - Prog/*.(f|F)90
      - Libraries/Modules/*.(f|F)90
  script:
      - export ALF_FLAGS_EXT="-g"
      - . configure.sh GNU serial
      - gfortran -v
      - make lib
      - make Hubbard_Plain_Vanilla
      - cp Prog/Hubbard_Plain_Vanilla.out Scripts_and_Parameters_files/Start
      - cd Scripts_and_Parameters_files/Start
      - valgrind --leak-check=full --log-file=vglog.txt ./Hubbard_Plain_Vanilla.out
      - cat vglog.txt
      - grep lost vglog.txt | cut -d":" -f 2 | cut -d " " -f 2 | f=$(cat); echo $(( ${f//$'\n'/+} ))

create_doc:
  image: aergus/latex
  stage: build
  artifacts:
    paths:
      - Documentation/doc.pdf
  script:
    - cd Documentation
    - latexmk -pdf doc.tex

create_doxygen:
  image: git.physik.uni-wuerzburg.de:25812/z03/pdi/debian:buster-clang
  stage: build
  artifacts:
    expire_in: 120 days
    paths:
      - doxygen/*
      - Images/*
  script:
      - apt-get update && apt install -y doxygen graphviz
      - doxygen doxygen/Doxyfile
