# Community guidelines
This document serves to describe the processes that outline our interaction with the community.
## Release cycle
ALF is an Open Source project available under a GPL license.
It has a major release cycle with a major release with an associated publication
every three years and minor releases in april and october of each year.
The current plan is ALF-2.0 in June-20 and ALF-3.0 in June-23.
The next minor release will be in Oct-20.

## Contributing
We gladly accept patches for bugfixes, portability enhancements or new features!

## Becoming a Core Developer
Someone who is already a core developer needs to create a public issue on our gitlab
that describes who you are and why you can contribute to the ALF Project in the long run.
Then we vote. You are accepted if all (the group of owners) agree.

## Getting access to the Development repository.
Someone who already has access must open a public issue on our gitlab that describes
why someone needs access to the development code. The other developers are required to declare
that their projects are not affected by the work of the new person.

## Reporting issues
Write a ticket!

## Getting support
Write us an E-Mail at alf@physik.uni-wuerzburg.de
